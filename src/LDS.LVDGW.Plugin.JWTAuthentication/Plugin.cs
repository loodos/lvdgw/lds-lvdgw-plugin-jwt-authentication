﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JWT;
using JWT.Builder;
using LDS.LVDGW.Plugin.UserAuthentication;

namespace LDS.LVDGW.Plugin.JWTAuthentication
{
    public class Plugin : PluginBase, IUserAuthenticationPlugin
    {
        //public override string Name => "JWTAuthenticationPlugin";

        [ParameterInfo("JWTSecret", "JWT Secret", Mandatory = true, ParameterType = ParameterTypes.Text, SampleValue = "<secret>")]
        public string JWTSecret { get; set; }

        public async Task<IDictionary<string, object>> ValidateAndRetrieveClaimsAsync(string token)
        {
            return await Task.Run(() =>
            {
                if (TryDecodeToken(token, out IDictionary<string, object> payload, out Exception exception))
                {
                    return payload;
                }
                else
                {
                    if (exception is JWT.TokenExpiredException)
                        throw new UserAuthentication.TokenExpiredException("Invalid token (Token has expired).", ((JWT.TokenExpiredException)exception).Expiration);
                    else if (exception is SignatureVerificationException)
                        throw new UserAuthentication.InvalidTokenException("Invalid token (Token has invalid signature).");
                    else if (exception is InvalidTokenPartsException)
                        throw new UserAuthentication.InvalidTokenException("Invalid token (Token has invalid parts).");
                    else
                        throw new UserAuthentication.InvalidTokenException("Invalid token.");
                }
            });
        }

        public async Task<bool> ValidateTokenAsync(string token)
        {
            return await Task.Run(() =>
            {
                return TryDecodeToken(token, out IDictionary<string, object> payload, out Exception exception);
            });
        }

        private bool TryDecodeToken(string token, out IDictionary<string, object> payload, out Exception exception)
        {
            try
            {
                token = token.Replace("Bearer ", string.Empty);
                payload = new JwtBuilder()
                    .WithSecret(JWTSecret)
                    .MustVerifySignature()
                    .Decode<IDictionary<string, object>>(token);

                exception = null;

                return true;
            }
            catch (Exception ex)
            {
                payload = null;
                exception = ex;
            }

            return false;
        }
    }
}
